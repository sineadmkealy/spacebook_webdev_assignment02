package models;

import java.util.Date;

import javax.persistence.*;
import play.db.jpa.*;

@Entity
public class Message extends Model // indicates information to be stored to a database
{
  public String messageSubject;
  public String messageText;
  public Date postedAt;


  @ManyToOne
  public User from;

  @ManyToOne
  public User to;

  public Message(User from, User to, String messageSubject, String messageText) // Story 8, Story 10 to add subject field
  {
    this.from = from;
    this.to = to;
    this.messageSubject = messageSubject;
    this.messageText = messageText;
  }
}