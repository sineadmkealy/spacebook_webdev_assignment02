package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import play.db.jpa.Blob;
import play.db.jpa.Model;

@Entity // indicates information to be stored to a database
// this User class represented by a table in the database
// individual User objects represented by each row in database table
public class User extends Model
{
  public String firstName;
  public String lastName;
  public String email;
  public String password;
  public int age;
  public String nationality;
  public String statusText; // field referred to in STORY 6
  
  //STORY 4 additional User fields introduced here, to be implemented in views/Profile/index.html
  public String genderText;
  public String relationshipText;
  public String petsText;
  public String hobbiesText;
  public String foodsText;
  public String likesText;
  
  public Blob   profilePicture;
  
  @OneToMany(mappedBy = "sourceUser")
  public List<Friendship> friendships = new ArrayList<Friendship>();
  // User list of friends =  a collection of friendship objects
  
  @OneToMany(mappedBy = "to")
  public List<Message> inbox = new ArrayList<Message>(); // inbox collection

  @OneToMany(mappedBy = "from")
  public List<Message> outbox = new ArrayList<Message>(); // outbox collection
  
 
  
  public User(String firstName, String lastName, String email, String password, int age, String nationality) //Story 2
  {
    this.firstName   = firstName;
    this.lastName    = lastName;
    this.email       = email;
    this.password    = password;
    this.age         = age; //Story 2
    this.nationality = nationality; //Story 2
  }
  
  public static User findByEmail(String email)
  {
    return find("email", email).first();
  }

  public boolean checkPassword(String password)
  {
    return this.password.equals(password);
  }  
  
  public void befriend(User friend)
  {
    Friendship friendship = new Friendship(this, friend);
    friendships.add(friendship);
    friendship.save();
    save();
  }

  public void unfriend(User friend)
  {
    Friendship thisFriendship = null;
    
    for (Friendship friendship:friendships)
    {
      if (friendship.targetUser== friend)
      {
        thisFriendship = friendship;
      }
    }
    friendships.remove(thisFriendship);
    thisFriendship.delete();
    save();
  }
  
  public void sendMessage (User to, String messageSubject, String messageText) // Story 10 add subject field
  {
    Message message = new Message (this, to, messageSubject, messageText); // create message object
    outbox.add(message); // add message object to sender (current logged-in User) outbox
    to.inbox.add(message); // add message object to recipient inbox
    message.save();
  }
}