package controllers;

import play.*;
import java.util.ArrayList;
import play.mvc.*;
import utils.MessageDateComparator;
import utils.MessageFromComparator;

import java.util.*;

import models.*;

public class Home extends Controller
{
  public static void index()
  {
    // STORY 7 User directed to Home screen if not logged in
    // Uses try-catch statement
    // could use if-else statement either
    try
    { // it tries to find the logged-in user, as follows
      String userId = session.get("logged_in_userid"); //code provided
      User user = User.findById(Long.parseLong(userId)); //code provided
      // skip to catch phrase at end of method

      // STORY 13 Sort messages by Date message sent, by Sender name, by
      // Conversation (date messages sent & received)
      String sort = params.get("sort");

      if (sort == null) // if no params passed though command line
      {
        sort = "date";
      }

      if (sort.equals("date")) // 1. SORT ALL MESSAGES BY DATE
      // sort an object so will throw null pointer exception
      {
        ArrayList<Message> messages = new ArrayList<Message>();
        // for each Message object in the user's inbox, add it to the messages array
        for (Message message : user.inbox)
        {
          messages.add(message);
        }
        Collections.sort(messages, new MessageDateComparator());
        // Sort all messages by date
        render(user, messages, sort); 
        // Render the view with the sorted messages
      }

      else
        if (sort.equals("sender")) // 2. SORT MESSAGES BY SENDER (IN DATE ORDER)
        {
          ArrayList<Message> messages = new ArrayList<Message>();
          for (Message message : user.inbox) 
            // for all messages involving that friend
          {
            messages.add(message);
          }
          // Call the MessageFromComparator on the messages ArrayList
          Collections.sort(messages, new MessageFromComparator());
          render(user, messages, sort); 
          // Render the view with the sorted messages
        }

        else
          if (sort.equals("conversation")) // 3. SORT MESSAGES BY CONVERSATION
                                           // (DATE MESSAGES SENT & RECEIVED)
          {
            ArrayList<ArrayList<Message>> conversations = new ArrayList<ArrayList<Message>>(); 
            // list of conversations
            // for each message in the user's inbox, add it to the messages array
            for (Friendship friendship : user.friendships)
            { // for each friend
              ArrayList<Message> conversation = new ArrayList<Message>(); 
              // a single conversation between 2 users

              for (Message message : user.inbox) 
                // all messages involving that friend
              {
                if (message.from.equals(friendship.targetUser) || message.to.equals(friendship.targetUser))
                  // if message to or from target user exists, add it to
                  // ArrayList of conversations
                  conversation.add(message);
              }
              // Call the MessageDateComparator on the conversations ArrayList
              Collections.sort(conversation, new MessageDateComparator());
              conversations.add(conversation);
            }

            render(user, conversations, sort); 
            // Render the view with the sorted messages
          }

     render(user);
    }
    catch (NumberFormatException e)
    {// if this fails and we get the exception
      // go to Accounts /login profile immediately
      Accounts.login();
    }
  }

  public static void drop(Long id)
  {
    String userId = session.get("logged_in_userid");
    User user = User.findById(Long.parseLong(userId));

    User friend = User.findById(id);

    user.unfriend(friend);
    Logger.info("Dropping " + friend.email);
    index();
  }
}