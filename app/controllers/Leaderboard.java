package controllers;
import play.*;
import play.mvc.*;
import utils.MessageDateComparator;
import utils.UserSocialComparator;
import utils.UserTalkativeComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import models.User;

// STORY 14 to create Social LeaderBoard list of members:- inbox size, outbox size and no. of friends 
public class Leaderboard extends Controller
{
  public static void index()
  {
    List<User> users = User.findAll();
    
    String sort = params.get("sort");// sort defined in Leaderbord.index html
    
    // When you go in via the menu, sort will be blank, so default to friends
    if (sort == null)
    {
    	sort = "friends";
    }

    // to determine the most social (most friends)
    if (sort.equals("friends")) // maps from Leaderboard index view
    {
    	Collections.sort(users, new UserSocialComparator());//Sort all users by friends
    }
    
    else
      // to determine the most talkative (the most messages sent)
    	if (sort.equals("talkative")) // maps from Leadership index view
    	{
    		Collections.sort(users, new UserTalkativeComparator());
    	}
    
    render(users);
  }
}
