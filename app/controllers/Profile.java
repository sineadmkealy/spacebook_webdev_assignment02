package controllers;

import play.*;
import play.db.jpa.Blob;
import play.mvc.*;

import java.util.*;

import models.*;

public class Profile extends Controller
{
  public static void index()
  {
    String userId = session.get("logged_in_userid");
    User user = User.findById(Long.parseLong(userId));
    render(user);
  }
  
  public static void changeStatus(String profiletext)
  {
    String userId = session.get("logged_in_userid");
    User user = User.findById(Long.parseLong(userId));
    user.statusText = profiletext;
    user.save();
    Logger.info("Status changed to " + profiletext);
    index();
  } 
  
  public static void changeProfile(String gender, String relationship, String pets, String hobbies, String foods, String likes)
  {
	  // STORY 4: this Profile CONTROLLER method maps User MODEL variables by routes change profile for Members VIEW
    String userId = session.get("logged_in_userid");
    User user = User.findById(Long.parseLong(userId));
    user.genderText = gender;
    user.relationshipText = relationship;
    user.petsText = pets;
    user.hobbiesText = hobbies;
    user.foodsText = foods;
    user.likesText = likes;
    user.save();
    Logger.info("Status changed to " + gender + relationship + pets + hobbies + foods + likes);
    index();
  } 
  
  public static void getPicture(Long id) 
  {
    User user = User.findById(id);
    Blob picture = user.profilePicture; 
    // STORY 9, used to provide picture with message list
    if (picture.exists())
    {
      response.setContentTypeIfNotSet(picture.type());
      renderBinary(picture.get());
    }
  }
  
  public static void uploadPicture(Long id, Blob picture)
  {
    User user = User.findById(id);
    user.profilePicture = picture;
    user.save();
    index();
  }   
}