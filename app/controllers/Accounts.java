

package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Accounts extends Controller
{
  public static void signup()
  {
    render();
  }

  public static void login()
  {
    render();
  }

  public static void logout()
  {
    session.clear();
    index();
  }
  
  public static void index()
  {
    render();
  }
  
  // Creates a new User object
  // By using user.firstName etc., the fields are automatically filled with data from form
  // Therefore, the controller action already takes an object
  //STORY 2 extend user model to include age and nationality
  public static void register(User user)
  {
    Logger.info(user.firstName + " " + user.lastName + " " + user.email + " " + user.password + " " + user.age + " " + user.nationality);
    user.save();
    
    index();
  }

  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" +  password);

    User user = User.findByEmail(email);
    if ((user != null) && (user.checkPassword(password) == true))
    {
      Logger.info("Authentication successful");
      session.put("logged_in_userid", user.id);
      Home.index();
    }
    else
    {
      Logger.info("Authentication failed");
      login();  
    }
  }
}