package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Members extends Controller
{
  public static void index()
  {
    List<User> users = User.findAll(); // returns list of all members in the database
    
     // STORY 5 shows a list of members(users) excluding the currently logged-in user
    String userId = session.get("logged_in_userid"); // Story 5 as provided in follow method below
    User me = User.findById(Long.parseLong(userId)); // Story 5 as provided in follow method below
    users.remove(me); // Story 5 removes the current user from User list before sending to the view
    render(users);
  }
  
  public static void follow(Long id)
  {
    User friend = User.findById(id); // use the id to search the database for friend
    
    String userId = session.get("logged_in_userid"); // get the currently logged-in user
    User me = User.findById(Long.parseLong(userId)); // translate into 'me'
    
    me.befriend(friend); // befriend the user
    Home.index(); // redisplay the home page
  }
}