package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class PublicProfile extends Controller
{
  public static void index()
  {
    render();
  }
  
  public static void visit(Long id)
  {
    User user = User.findById(id);
    Logger.info("Just visiting the page for " + user.firstName + ' ' + user.lastName );
    render(user); // currently-logged in user passed to public profile view
  }  
  
  public static void sendMessage(Long id, String messageSubject, String messageText)//Story 10 add subject
  {
    String userId = session.get("logged_in_userid");
    User fromUser = User.findById(Long.parseLong(userId));
    User toUser = User.findById(id);

    Logger.info("Message from user " + 
        fromUser.firstName + ' ' + fromUser.lastName +" to " +
        toUser.firstName + ' ' + toUser.lastName +": " +
        messageText);    

    fromUser.sendMessage(toUser, messageSubject, messageText); //Story 10 add subject
    visit(id);
  } 
}