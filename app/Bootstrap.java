
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import play.*;
import play.db.jpa.Blob;
import play.jobs.*;
import play.libs.MimeTypes;
import play.test.*;

import models.*;

@OnApplicationStart // to preload the database with content in the data.yml file
public class Bootstrap extends Job
{
  public void doJob()
  {
    Fixtures.deleteDatabase();
    Fixtures.loadModels("data.yml"); // to preload data

    //To preload the app images:
    String photoName1 = "homer.gif"; // image fiel located in the app folder
    Blob blob1 = new Blob(); //to create an empty blob object blob1
    try
    {
      blob1.set(new FileInputStream(photoName1), MimeTypes.getContentType(photoName1));
      // read the image and store it in the blob object
    }
    catch (FileNotFoundException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    User homer = User.findByEmail("homer@simpson.com"); // to locate the User model object 'homer'
    homer.profilePicture = blob1; // to set the blob in that mode
    homer.save(); // save the model

    String photoName2 = "lisa.gif";
    Blob blob2 = new Blob();
    try
    {
      blob2.set(new FileInputStream(photoName2), MimeTypes.getContentType(photoName2));
    }
    catch (FileNotFoundException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    User lisa = User.findByEmail("lisa@simpson.com");
    lisa.profilePicture = blob2;
    lisa.save();

    String photoName3 = "marge.gif";
    Blob blob3 = new Blob();
    try
    {
      blob3.set(new FileInputStream(photoName3), MimeTypes.getContentType(photoName3));
    }
    catch (FileNotFoundException e)
    {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    User marge = User.findByEmail("marge@simpson.com");
    marge.profilePicture = blob3;
    marge.save();

  }
}