package utils;

import java.util.Comparator;
import java.util.Date;

import models.Message;
import models.User;

public class MessageDateComparator implements Comparator<Message>
{
  @Override
  public int compare(Message a, Message b)
  {
	//TODO: Complete implementation of method MessageDateComparator.compare
    //Algorithmic code: delete when method complete 
    //compare the time-date attributes of each message
    //use the Date compareTo method
    //use appropriate attribute of Message b as the parameter
	  
		Date aDate = a.postedAt ; // //compare the time-date attributes of first message object
		Date bDate = b.postedAt ; //second message object
		return aDate.compareTo(bDate);

  }
}

